from subprocess import Popen, PIPE
import os

for filename in os.listdir("img2"):
    if filename.endswith(".bmp"): 
        process = Popen(["./main", "img2/" + filename, "rgb"], stdout=PIPE)
        (output, err) = process.communicate()
        print(output)
        exit_code = process.wait()
