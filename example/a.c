#include <stdio.h>
#include <math.h>
#include "../libbmp.h"
#include <omp.h>
#include <string.h>

#define GRAYSCALE_RED_COF 0.299
#define GRAYSCALE_GREEN_COF 0.527
#define GRAYSCALE_BLUE_COF 0.114

bmp_img grayscale_transform_p(bmp_img *img) 
{
	const size_t h = img->img_header.biHeight;
	const size_t w = img->img_header.biWidth;

    bmp_img img2;
	bmp_img_init_df(&img2, w, h);

    #pragma omp paralell for
	for (size_t y = 0; y < h; ++y) 
	{
		for (size_t x = 0; x < w; ++x)
		{
			bmp_pixel grayscale_pixel;
			bmp_pixel current_pixel = img->img_pixels[y][x]; 

			unsigned char gray = (current_pixel.red * GRAYSCALE_RED_COF 
				+ current_pixel.green * GRAYSCALE_GREEN_COF 
				+ current_pixel.blue * GRAYSCALE_BLUE_COF);

			grayscale_pixel.blue = gray;
			grayscale_pixel.red = gray;
			grayscale_pixel.green = gray;

			img2.img_pixels[y][x] = grayscale_pixel;
		}
	}

    return img2;
}

bmp_img grayscale_transform(bmp_img *img) 
{
	const size_t h = img->img_header.biHeight;
	const size_t w = img->img_header.biWidth;

    bmp_img img2;
	bmp_img_init_df(&img2, w, h);

	for (size_t y = 0; y < h; ++y) 
	{
		for (size_t x = 0; x < w; ++x)
		{
			bmp_pixel grayscale_pixel;
			bmp_pixel current_pixel = img->img_pixels[y][x]; 

			unsigned char gray = (current_pixel.red * GRAYSCALE_RED_COF 
				+ current_pixel.green * GRAYSCALE_GREEN_COF 
				+ current_pixel.blue * GRAYSCALE_BLUE_COF);

			grayscale_pixel.blue = gray;
			grayscale_pixel.red = gray;
			grayscale_pixel.green = gray;

			img2.img_pixels[y][x] = grayscale_pixel;
		}
	}

    return img2;
}


static bmp_pixel get_pixel_average(bmp_img *img, size_t y, size_t x)
{
	bmp_pixel p1 = img->img_pixels[y - 1][x - 1];
	bmp_pixel p2 = img->img_pixels[y - 1][x];
	bmp_pixel p3 = img->img_pixels[y - 1][x + 1];
	bmp_pixel p4 = img->img_pixels[y][x - 1];
	bmp_pixel p5 = img->img_pixels[y][x + 1];
	bmp_pixel p6 = img->img_pixels[y + 1][x - 1];
	bmp_pixel p7 = img->img_pixels[y + 1][x];
	bmp_pixel p8 = img->img_pixels[y + 1][x + 1];
	// bmp_pixel p9 = img->img_pixels[y][x];

	unsigned char red_avg = (p1.red + p2.red + p3.red + p4.red 
		+ p5.red + p6.red + p7.red + p8.red) / 8;

	unsigned char green_avg = (p1.green + p2.green + p3.green + p4.green 
		+ p5.green + p6.green + p7.green + p8.green ) / 8;

	unsigned char blue_avg = (p1.blue + p2.blue + p3.blue + p4.blue 
		+ p5.blue + p6.blue + p7.blue + p8.blue) / 8;


	bmp_pixel avg_pixel;
	avg_pixel.red = red_avg;
	avg_pixel.green = green_avg;
	avg_pixel.blue = blue_avg;

	return avg_pixel;

}


// basic blur implemented using kernel convolution
bmp_img blur_image_p(bmp_img *img)
{

	const size_t h = img->img_header.biHeight;
	const size_t w = img->img_header.biWidth;

	bmp_img blured_img;
	bmp_img_init_df(&blured_img, w, h);

	// blur main parts of the image
    #pragma omp paralell for
	for (size_t y = 0 ; y < h ; ++y)
	{
		for (size_t x = 0; x < w; ++x)
		{
			// skip the borders
			if (y > 1 && y < h - 1 && x > 1 && x < w - 1)
			{
				bmp_pixel avg_pixel = get_pixel_average(img, y, x);
				blured_img.img_pixels[y][x] = avg_pixel;
			}
			else
			{
				blured_img.img_pixels[y][x] = img->img_pixels[y][x];
			}
		}
	}

	return blured_img;
}

// basic blur implemented using kernel convolution
bmp_img blur_image(bmp_img *img)
{

	const size_t h = img->img_header.biHeight;
	const size_t w = img->img_header.biWidth;

	bmp_img blured_img;
	bmp_img_init_df(&blured_img, w, h);

	// blur main parts of the image
 	for (size_t y = 0 ; y < h ; ++y)
	{
		for (size_t x = 0; x < w; ++x)
		{
			// skip the borders
			if (y > 1 && y < h - 1 && x > 1 && x < w - 1)
			{
				bmp_pixel avg_pixel = get_pixel_average(img, y, x);
				blured_img.img_pixels[y][x] = avg_pixel;
			}
			else
			{
				blured_img.img_pixels[y][x] = img->img_pixels[y][x];
			}
		}
	}

	return blured_img;
}

bmp_img rotate_image_p(bmp_img *img)
{
	const size_t h = img->img_header.biHeight;
	const size_t w = img->img_header.biWidth;

	bmp_img rotated;
	bmp_img_init_df(&rotated, h, w);

    #pragma omp paralell for
	for (size_t y = 0; y < h; ++y)
	{
		for (size_t x = 0; x < w; ++x)
		{
			rotated.img_pixels[x][y] = img->img_pixels[y][x];
		}
	}

	return rotated;
}

bmp_img rotate_image(bmp_img *img)
{
	const size_t h = img->img_header.biHeight;
	const size_t w = img->img_header.biWidth;

	bmp_img rotated;
	bmp_img_init_df(&rotated, h, w);


	for (size_t y = 0; y < h; ++y)
	{
		for (size_t x = 0; x < w; ++x)
		{
			rotated.img_pixels[x][y] = img->img_pixels[y][x];
		}
	}

	return rotated;
}

int main(int argc, char **argv)
{

    char *path = argv[1];
    char *ops = argv[2];
    int len = strlen(ops);
    printf("%s\n", path);

	bmp_img img1;
	bmp_img_read(&img1, path);

    for (int i = 0; i < 3; ++i)
    {
        double start, end;
        for (int i = 0; i < len; ++i)
        {
            if (ops[i] == 'g')
            {
                
                start = omp_get_wtime();
                img1 = grayscale_transform(&img1);
                end = omp_get_wtime();
                printf("[g-normal] %lf \n", end - start);

                start = omp_get_wtime();
                grayscale_transform_p(&img1);
                end = omp_get_wtime();
                printf("[g-p1] %lf \n", end - start);
            }
            else if (ops[i] == 'r')
            {
                start = omp_get_wtime();
                img1 = rotate_image(&img1);
                end = omp_get_wtime();
                printf("[r-normal] %lf \n", end - start);


                start = omp_get_wtime();
                rotate_image_p(&img1);
                end = omp_get_wtime();
                printf("[r-p1] %lf \n", end - start);
            }
            else
            {
                start = omp_get_wtime();
                img1 = blur_image(&img1);
                end = omp_get_wtime();
                printf("[b-p1] %lf \n", end - start);


                start = omp_get_wtime();
                blur_image_p(&img1);
                end = omp_get_wtime();
                printf("[b-p1] %lf \n", end - start);
            }
        }
    	printf("===================================================================\n");
    }

    char file_result_name[100];
    file_result_name[0] = '\0';

    strcat(file_result_name, path);
    strcat(file_result_name, "_result.bmp");

	bmp_img_write(&img1, file_result_name);

	bmp_img_free(&img1);

	return 0;
}
